package com.vishalgautam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HelloMaven {

//    Define a logging variable
    private final static Logger LOG = LoggerFactory.getLogger(HelloMaven.class);

    public static void main(String[] args) {

//        Adding different levels of logging
        LOG.info("Logging info");
        LOG.debug("Logging debug");
    }

}
